package LocatorsAndMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.common.methods.CommonMethods;
import com.mainMethod.Driver;


public class InputForms {
	public WebElement txtPEYMsg = Driver.d.findElement(By.id("user-message"));
	public WebElement btnShowMsg = Driver.d.findElement(By.xpath("//button[text()='Show Message']"));
	public WebElement txtDisplay = Driver.d.findElement(By.id("display"));
	public WebElement txtSum1 = Driver.d.findElement(By.id("sum1"));
	public WebElement txtSum2 = Driver.d.findElement(By.id("sum2"));
	public WebElement btnGetTotal = Driver.d.findElement(By.xpath("//button[text()='Get Total']"));
	public WebElement txtDisplayVal = Driver.d.findElement(By.id("displayvalue"));
	
	public void handleInputForms() throws Exception{
		try{
			if((CommonMethods.enterText(txtPEYMsg, "This is selenium testing")).equals("False")) {
				System.out.println("Script failed to enter text in textbox field");
			}
			if((CommonMethods.clickButton(btnShowMsg)).equals("False")){
				System.out.println("Script failed to click on Show Message button");
			}else{
				System.out.println("Text entered is: "+ txtDisplay.getText());
			}
		}catch(Exception e){
			System.out.println("e.getMessage()");
		}
		
	}
	
	
	public void handleTwoInputForms() throws Exception{
		try{
			if((CommonMethods.enterText(txtSum1, "10")).equals("False")) {
				System.out.println("Script failed to enter text in 'Enter a value' field");
			}
			
			if((CommonMethods.enterText(txtSum2, "20")).equals("False")) {
				System.out.println("Script failed to enter text in 'Enter b value' field");
			}
			
			if((CommonMethods.clickButton(btnGetTotal)).equals("False")){
				System.out.println("Script failed to click on GetTotal button");
			}else{
				System.out.println("Sum is: "+ txtDisplayVal.getText());
			}
			
		}catch(Exception e){
			System.out.println("e.getMessage()");
		}
	}


}

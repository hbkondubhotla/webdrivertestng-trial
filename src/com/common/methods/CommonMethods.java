package com.common.methods;

import org.openqa.selenium.WebElement;

public class CommonMethods {
	
	public static String enterText(WebElement txtbox,String msg) throws Exception{
		String bln="False";
		try{
		if(txtbox.isDisplayed()){
		txtbox.sendKeys(msg);
		String result = txtbox.getAttribute("value");
		if(result.equalsIgnoreCase(msg)){
			bln = "True";
		}else{
			bln="False";
		}
		}
		}catch(Exception e){
			System.out.println(e.getMessage());
			return "False";
		}
		
		return bln;
		
	}
	
	public static String clickButton(WebElement btn){
		String bln="False";
		if (btn.isDisplayed()){
			bln="True";
			btn.click();
		}else{
			bln = "False";
		}
		return bln;
	}

}

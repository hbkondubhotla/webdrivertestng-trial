package com.mainMethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Driver {
	
	public static WebDriver d;
/*	public static void main(String args[]) throws Exception{
		chromeDriver();
		d.get("http://www.seleniumeasy.com/test/basic-first-form-demo.html");
		InputForms inf = new InputForms();
		inf.handleInputForms();
		inf.handleTwoInputForms();
		Driver d = new Driver();
		d.test();
	}*/
	@Test(groups={"webdrivertest"})
	public static void chromeDriver(){
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Workspace\\com.webdriver.testng\\Resources\\chromedriver.exe");
		d = new ChromeDriver();
		d.get("https://www.google.com");
		d.quit();
	}
	public void test()
	{
		System.out.println(this.getClass().getClassLoader().getResource("chromedriver.exe").getFile());
	}
}
